/**
 * @author Yisen <yisen.n@gmail.com> QQ:179338996
 * @date 2012-11-20
 * @copyright GNU Public License.
 *
 * @brief
 */

#include <QtGui>
#include <QUrl>
#include <QtXml>
#include "mainwindow.h"
#include "ossapi.h"


int main(int argc, char *argv[])
{
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    OssAPI oss(HOST, ACCESS_ID, SECRET_ACCESS_KEY);

    //add bucket
//    qDebug() << oss.AddBucket("yisen", "");

    //delete bucket
//    qDebug() << oss.DeleteBucket("yisen");

    //get bucket list
//    QList<OssBucket> bucketList = oss.BucketList();
//    for (int i = 0; i < bucketList.length(); i++)
//    {
//        OssBucket bucket = bucketList.at(i);
//        qDebug() << bucket.Name << bucket.CreationDate;
//        qDebug() << bucket.Owner.ID << bucket.Owner.DisplayName;
//    }

    //get object list
//    QList<OssObject> objectList;
//    objectList = oss.ObjectList("yisenet");
//    qDebug() << objectList.at(0).key << objectList.at(0).Size;

    //download object
//    oss.ObjectDownload("yisenet", "testfile.txt", 21, "");

    //upload object
//    qDebug() << oss.UploadObject("yisenet", "testfile2.txt");

    //delete object
    qDebug() << oss.DeleteObject("yisenet", "testfile");

    return a.exec();
}
