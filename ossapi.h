/**
 * @author Yisen <yisen.n@gmail.com> QQ:179338996
 * @date 2012-11-26
 * @copyright GNU Public License.
 *
 * @brief Oss API
 */

#ifndef OSSAPI_H
#define OSSAPI_H

#include <QObject>
#include <QString>
#include <QHash>
#include <QDebug>
#include <QUrl>
#include <QDateTime>
#include <QLocale>
#include <QCryptographicHash>
#include <QtNetwork>
#include <QtXml>

extern const QString HOST;
extern const QString HOST_URL;
extern const QString ACCESS_ID;
extern const QString SECRET_ACCESS_KEY;

struct OssOwner
{
    QString ID;
    QString DisplayName;
};

struct OssBucket
{
    QString Name;
    QString CreationDate;
    OssOwner Owner;
};

struct OssObject
{
    QString key;
    QString LastModified;
    QString ETag;
    QString Type;
    QString Size;
    QString StorageClass;
    OssOwner Owner;
};


class OssAPI : public QObject
{
    Q_OBJECT
    
public:
    explicit OssAPI(QString host, QString accessId, QString secretAccessKey,
                    int port=80, bool isSecurity=false, QObject *parent = 0);

    enum SignatureType
    {
        HEAD,
        URL
    };

    QString Host;
    QString HostUrl;
    QString AccessId;
    QString SecretAccessKey;
    int Port;
    bool IsSecurity;
    QString LastErrorMsg;

    QString DefaultContentType;
    QString Provider;
    int SendBufferSize;
    int GetBufferSize;


    /**
     * list all buckets
     *
     * @return bucket list
     */
    QList<OssBucket> BucketList();

    /**
     * list all objects in this bucket
     *
     * @param[in] bucket bucket name
     *
     * @return object list
     */
    QList<OssObject> ObjectList(QString bucket);


    /**
     * download object to file
     *
     * @param[in] bucket bucket name
     * @param[in] objectName object name
     * @param[in] objectSize object size
     * @param[in] savePath where downloaded file to save
     *
     * @return
     *          * 1:    download success
     *          * -1:   open file error
     *          * -2:   file read error
     */
    int DownloadObject(QString bucket, QString objectName,
                       qint64 objectSize, QString savePath);

    /**
     * add new bucket
     *
     * @param[in] bucketName bucket name
     * @param[in] acl acl permission
     *              * "public-read-write"
     *              * "public-read"
     *              * "" for "private"
     *
     * @return
     *          * 200:  add new bucket success
     */
    int AddBucket(QString bucketName, QString acl);

    /**
     * delete bucket
     *
     * @param[in] bucketName bucket name
     *
     * @return
     *          * 1:    delete bucket success
     *          * 203:  ContentNotFoundError
     */
    int DeleteBucket(QString bucketName);

    /**
     * upload object
     *
     * @param[in] bucketName bucket name
     * @param[in] uploadFilePath file path
     *
     * @return
     *          * 1:    download success
     */
    int UploadObject(QString bucketName, QString uploadFilePath);

    /**
     * delete object
     *
     * @param[in] bucketName bucket name
     * @param[in] objectName object name
     *
     * @return
     *          * 1:    delete success
     */
    int DeleteObject(QString bucketName, QString objectName);

private slots:

private:
    QNetworkAccessManager *networkManager;
    QNetworkReply *reply;

    /**
     * list all buckets
     *
     * @return bucket list xml
     */
    QString getService();

    /**
     * list all objects in this bucket
     *
     * @param[in] bucket bucket name
     *
     * @return object list xml
     */
    QString getBucket(QString bucket);

    /**
     * get object's data
     *
     * @param[in] bucket bucket name
     * @param[in] object object name
     *
     * @return object data xml
     */
    QString getObject(QString bucket, QString object);

    /**
     * add new bucket
     *
     * @param[in] bucketName bucket name
     * @param[in] acl acl permission
     *
     * @return html response
     */
    QString putBucket(QString bucketName, QString acl);

    /**
     * delete bucket
     *
     * @param[in] bucketName bucket name
     *
     * @return html response
     */
    QString deleteBucket(QString bucketName);

    /**
     * upload object
     *
     * @param[in] bucketName bucket name
     * @param[in] uploadFilePath file path
     *
     * @return html response
     */
    QString putObject(QString bucketName, QString uploadFilePath);

    /**
     * delete object
     *
     * @param[in] bucketName bucket name
     * @param[in] objectName object name
     *
     * @return html response
     */
    QString deleteObject(QString bucketName, QString objectName);

    /**
     * build http request header
     *
     * @param[in] method request method
     * @param[in] bucket bucket name
     * @param[in] object object name
     * @param[in] headers headers hash
     * @param[out] body output data body
     * @param[in] params params hash

     * @return void
     */
    void buildHeader(QString method, QString bucket,
                        QString object, QHash<QString, QString> &headers,
                        QString body, QHash<QString, QString> &params);


    /**
     * build http request url
     *
     * @param[in] method request method
     * @param[in] bucket bucket name
     * @param[in] object object name
     * @param[in] headers headers hash
     * @param[out] body output data body
     * @param[in] params params hash

     * @return http request url
     */
    QString buildUrl(QString method, QString bucket, QString object,
                     QHash<QString, QString> &headers, QString body,
                     QHash<QString, QString> &params);

    QByteArray httpRequest(QString method, QString bucket, QString object, QString body,
                     QHash<QString, QString> &headers);
    QString getDate();
    QString getAuthorization(QString method, QHash<QString, QString> &headers,
                           QString resource);

    /**
     * build signature
     *
     * @param[in] method request method
     * @param[in] headers request headers hash
     * @param[in] resource request resource string
     *
     * @return signature string
     */
    QString buildSignature(QString method, QHash<QString, QString> &headers,
                           QString resource);

    /**
     * hmac-sha1 encode function
     *
     * @param[in]   secretKey secret key
     * @param[in]   baseString string to encode
     * @return  encoded string
     */
    QString hmacSha1(QByteArray key, QByteArray baseString);

    QString buildCanonicalizedOSSHeaders(QHash<QString, QString> &headers);

};

#endif // OSSAPI_H
