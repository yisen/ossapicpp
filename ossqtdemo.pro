#-------------------------------------------------
#
# Project created by QtCreator 2012-10-30T17:16:39
#
#-------------------------------------------------

QT       += core gui network xml

TARGET = ossqtdemo
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    ossapi.cpp

HEADERS  += mainwindow.h \
    ossapi.h
