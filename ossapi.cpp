#include "ossapi.h"

const QString HOST = "oss.aliyuncs.com";
const QString HOST_URL = "http://oss.aliyuncs.com";
const QString ACCESS_ID = "MKfU8v40zo5wK4Kp";
const QString SECRET_ACCESS_KEY = "zeCeVQWl9yDrdEgg2qumQSHifuuQYo";


OssAPI::OssAPI(QString host, QString accessId, QString secretAccessKey,
               int port, bool isSecurity, QObject *parent) :
    QObject(parent)
{
    Host = host;
    AccessId = accessId;
    SecretAccessKey = secretAccessKey;
    Port = port;
    IsSecurity = isSecurity;

    DefaultContentType = "application/octet-stream";
    SendBufferSize = 8192;
    GetBufferSize = 1024 * 1024 * 10;
    Provider = "OSS";

    networkManager = new QNetworkAccessManager();
    reply = NULL;
}

QString OssAPI::getService()
{
    QString method = "GET";
    QString bucket = "";
    QString object = "";
    QString body = "";
    QHash<QString, QString> headers;
    QHash<QString, QString> params;

    buildHeader(method, bucket, object, headers, body, params);
    return QString(httpRequest(method, bucket, object, body, headers));
}

QString OssAPI::getBucket(QString bucket)
{
    QString method = "GET";
    QString object = "";
    QString body = "";
    QHash<QString, QString> headers;
    QHash<QString, QString> params;

    buildHeader(method, bucket, object, headers, body, params);
    return QString(httpRequest(method, bucket, object, body, headers));
}

QString OssAPI::getObject(QString bucket, QString object)
{
    QString method = "GET";
    QString body = "";
    QHash<QString, QString> headers;
    QHash<QString, QString> params;

    buildHeader(method, bucket, object, headers, body, params);
    return QString(httpRequest(method, bucket, object, body, headers));
}

QString OssAPI::putBucket(QString bucketName, QString acl)
{
    QString method = "PUT";
    QString bucket = bucketName;
    QString object = "";
    QString body = "";
    QHash<QString, QString> headers;
    QHash<QString, QString> params;

    if (acl.length() != 0)
        headers["x-oSS-acl"] = acl;

    buildHeader(method, bucket, object, headers, body, params);
    return QString(httpRequest(method, bucket, object, body, headers));
}

QString OssAPI::deleteBucket(QString bucketName)
{
    QString method = "DELETE";
    QString bucket = bucketName;
    QString object = "";
    QString body = "";
    QHash<QString, QString> headers;
    QHash<QString, QString> params;

    buildHeader(method, bucket, object, headers, body, params);
    return QString(httpRequest(method, bucket, object, body, headers));
}

QString OssAPI::putObject(QString bucketName, QString uploadFilePath)
{
    QFileInfo fileInfo(uploadFilePath);
    QString errorStr = "Error:";

    if (!fileInfo.exists())
    {
        errorStr += "-1";
        LastErrorMsg = "File path error";
        return errorStr;
    }

    QFile file(fileInfo.absoluteFilePath());
    if (!file.open(QIODevice::ReadOnly))
    {
        errorStr += "-2";
        LastErrorMsg = "File open error";
        return errorStr;
    }

    QString body;
    QTextStream in(&file);
    body = in.readAll();

    QString method = "PUT";
    QString bucket = bucketName;
    QString object = fileInfo.fileName();
    QHash<QString, QString> headers;
    QHash<QString, QString> params;

    QString contentTypeHeader;
    QString contentType = fileInfo.suffix().toLower();

    if (contentType == "txt" || contentType.length() == 0)
        contentTypeHeader = "text/plain; ";
    else if (contentType == "html")
        contentTypeHeader = "text/html; ";

    headers["Content-Length"] = fileInfo.size();
    headers["Content-Type"] = contentTypeHeader + "charset=UTF-8";

    buildHeader(method, bucket, object, headers, body, params);
    return QString(httpRequest(method, bucket, object, body, headers));
}

QString OssAPI::deleteObject(QString bucketName, QString objectName)
{
    QString method = "DELETE";
    QString bucket = bucketName;
    QString object = objectName;
    QString body = "";
    QHash<QString, QString> headers;
    QHash<QString, QString> params;

    buildHeader(method, bucket, object, headers, body, params);
    return QString(httpRequest(method, bucket, object, body, headers));
}

QList<OssBucket> OssAPI::BucketList()
{
    QList<OssBucket> bucketList;
    QDomDocument doc;
    doc.setContent(this->getService());

    QDomElement root = doc.documentElement();
    OssOwner owner;

    //get owner
    QDomNodeList ownerList = root.elementsByTagName("Owner");
    QDomNodeList ownerNodes = ownerList.at(0).childNodes();

    owner.ID = ownerNodes.at(0).firstChild().nodeValue();
    owner.DisplayName = ownerNodes.at(1).firstChild().nodeValue();


    //get buckets
    QDomNodeList nodeList = root.elementsByTagName("Bucket");

    for (int i = 0; i < nodeList.count(); i++)
    {
        QDomNodeList bucketNodes = nodeList.at(i).childNodes();

        OssBucket bucket;
        bucket.Name = bucketNodes.at(0).firstChild().nodeValue();
        bucket.CreationDate = bucketNodes.at(1).firstChild().nodeValue();
        bucket.Owner = owner;
        bucketList.append(bucket);
    }

    return bucketList;
}

QList<OssObject> OssAPI::ObjectList(QString bucket)
{
    QList<OssObject> objectList;
    QDomDocument doc;
    doc.setContent(this->getBucket(bucket));

    QDomElement root = doc.documentElement();
    QDomNodeList objectNodeList = root.elementsByTagName("Contents");

    for (int i = 0; i < objectNodeList.count(); i++)
    {
        OssObject ossObject;
        QDomNodeList objectNodes = objectNodeList.at(i).childNodes();

        ossObject.key = objectNodes.at(0).firstChild().nodeValue();
        ossObject.LastModified = objectNodes.at(1).firstChild().nodeValue();
        ossObject.ETag = objectNodes.at(2).firstChild().nodeValue();
        ossObject.Type = objectNodes.at(3).firstChild().nodeValue();
        ossObject.Size = objectNodes.at(4).firstChild().nodeValue();
        ossObject.StorageClass = objectNodes.at(5).firstChild().nodeValue();

        OssOwner owner;
        QDomNodeList ownerNodes = objectNodes.at(6).childNodes();
        owner.ID = ownerNodes.at(0).firstChild().nodeValue();
        owner.DisplayName = ownerNodes.at(1).firstChild().nodeValue();
        ossObject.Owner = owner;

        objectList.append(ossObject);
    }

    return objectList;
}

int OssAPI::DownloadObject(QString bucket, QString objectName,
                           qint64 objectSize, QString savePath)
{
    int returnValue = 0;
    QString objectDate = this->getObject(bucket, objectName);

    if (objectSize != objectDate.length())
    {
        LastErrorMsg = "File read error";
        returnValue = -2;
        return returnValue;
    }

    QString filePath = savePath + objectName;
    QFile saveFile(filePath);
    if (!saveFile.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        LastErrorMsg = "Open file error";
        returnValue = -1;
        return returnValue;
    }

    QTextStream out(&saveFile);
    out << objectDate;
    saveFile.close();

    returnValue = 1;
    return returnValue;
}

int OssAPI::AddBucket(QString bucketName, QString acl)
{
    int returnValue = 0;

    if (acl.length() == 0)
        acl = "private";

    QString responseXml = this->putBucket(bucketName, acl);

    if (responseXml.contains("Error"))
    {
        returnValue = responseXml.mid(6).toInt();
        LastErrorMsg = "Error";
        return returnValue;
    }

    returnValue = 200;
    return returnValue;
}

int OssAPI::DeleteBucket(QString bucketName)
{
    int returnValue = 0;
    QString responseXml = this->deleteBucket(bucketName);

    if (responseXml.contains("Error"))
    {
        returnValue = responseXml.mid(6).toInt();

        if (returnValue == 404)
            LastErrorMsg = "No such bucket";
        else
            LastErrorMsg = "Error";
        return returnValue;
    }

    returnValue = 200;
    return returnValue;
}

int OssAPI::UploadObject(QString bucketName, QString uploadFilePath)
{
    int returnValue = 0;
    QString responseXml = this->putObject(bucketName, uploadFilePath);

    if (responseXml.contains("Error"))
    {
        returnValue = responseXml.mid(6).toInt();

        if (returnValue == 404)
            LastErrorMsg = "No such bucket";
        else
            LastErrorMsg = "Error";
        return returnValue;
    }

    returnValue = 1;
    return returnValue;
}

int OssAPI::DeleteObject(QString bucketName, QString objectName)
{
    int returnValue = 0;
    QString responseXml = this->deleteObject(bucketName, objectName);

    if (responseXml.contains("Error"))
    {
        returnValue = responseXml.mid(6).toInt();

        if (returnValue == 404)
            LastErrorMsg = "No such bucket";
        else if (returnValue == 204)
            LastErrorMsg = "No such object";
        else
            LastErrorMsg = "Error";
        return returnValue;
    }

    returnValue = 1;
    return returnValue;
}

void OssAPI::buildHeader(QString method, QString bucket, QString object,
                         QHash<QString, QString> &headers, QString body,
                         QHash<QString, QString> &params)
{
    QString httpPrefix;
    if (IsSecurity)
        httpPrefix = "https://";
    else
        httpPrefix = "http://";

    QString resource = "/";
    if (bucket.length() == 0)
    {
        headers["Host"] = Host;
        HostUrl = httpPrefix + headers["Host"];
    }
    else
    {
        headers["Host"] = bucket + "." + Host;
        resource = resource + bucket + "/";
        HostUrl = httpPrefix + headers["Host"];

        if (object.length() != 0)
        {
            resource += object;
            HostUrl = HostUrl + "/" + object;
        }
    }

    headers["Date"] = getDate();
    headers["Authorization"] = getAuthorization(method, headers, resource);
}


//TODO
QString OssAPI::buildUrl(QString method, QString bucket, QString object,
                         QHash<QString, QString> &headers, QString body,
                         QHash<QString, QString> &params)
{
    QString url;
    return url;
}

QByteArray OssAPI::httpRequest(QString method, QString bucket, QString object,
                               QString body, QHash<QString, QString> &headers)
{
    QNetworkRequest request;

    QHashIterator<QString, QString> iter(headers);
    while (iter.hasNext())
    {
        iter.next();
        if (iter.value().length() == 0)
            continue;
        request.setRawHeader(iter.key().toAscii(), iter.value().toAscii());
    }
    request.setUrl(QUrl(HostUrl));

    QByteArray responseData;
    QEventLoop eventLoop;

    QObject::connect(networkManager, SIGNAL(finished(QNetworkReply*)),
                     &eventLoop, SLOT(quit()));

    if (method == "GET")
        reply = networkManager->get(request);
    else if (method == "PUT")
        reply = networkManager->put(request, body.toAscii());
    else if (method == "DELETE")
        reply = networkManager->deleteResource(request);

    eventLoop.exec();

    int httpStatusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();

    if (httpStatusCode != 200)
    {
        QString errorStr = "Error:" + QString::number(httpStatusCode);
//        responseData = errorStr.toAscii() + "\n";
        responseData = errorStr.toAscii();
    }
    else
        responseData = reply->readAll();

    delete reply;
    reply = NULL;

    return responseData;
}

QString OssAPI::getDate()
{
    QDateTime currentTime = QDateTime::currentDateTime();
    QString format ="ddd, dd MMM yyyy hh:mm:ss";
    QLocale locale = QLocale(QLocale::English, QLocale::UnitedKingdom);

    QString returnTime;
    returnTime = locale.toString(currentTime.toUTC(), format);

    return returnTime + " GMT";
}

QString OssAPI::getAuthorization(QString method,
                                 QHash<QString, QString> &headers,
                                 QString resource)
{
    QString authorizationString = Provider + " " + AccessId + ":";
    return authorizationString + buildSignature(method, headers, resource);
}

QString OssAPI::buildSignature(QString method, QHash<QString, QString> &headers,
                               QString resource)
{
    QString stringToSignature = method + "\n";

    stringToSignature += headers["Content-Md5"];
    stringToSignature += "\n";
    stringToSignature += headers["Content-Type"];
    stringToSignature += "\n";
    stringToSignature += headers["Date"];
    stringToSignature += "\n";

    //TODO:add CanonicalizedOSSHeaders
    stringToSignature += buildCanonicalizedOSSHeaders(headers);
    stringToSignature += resource;

    return hmacSha1(SecretAccessKey.toAscii(), stringToSignature.toAscii());
}

//TODO:
QString OssAPI::buildCanonicalizedOSSHeaders(QHash<QString, QString> &headers)
{
    QString ossHeadersString;
    QMap<QString, QString> ossHeaders;

    QHashIterator<QString, QString> iter(headers);
    while (iter.hasNext())
    {
        iter.next();
        if (iter.value().length() == 0)
            continue;
        if (iter.key().contains("x-oss-", Qt::CaseInsensitive))
            ossHeaders[iter.key().toLower()] = iter.value();
    }

    QMapIterator<QString, QString> mapIter(ossHeaders);
    while (mapIter.hasNext())
    {
        mapIter.next();
        ossHeadersString = ossHeadersString + mapIter.key() + ":" + mapIter.value() + "\n";
    }

    return ossHeadersString;
}

QString OssAPI::hmacSha1(QByteArray key, QByteArray baseString)
{
    int blockSize = 64; // HMAC-SHA-1 block size, defined in SHA-1 standard
    if (key.length() > blockSize) { // if key is longer than block size (64), reduce key length with SHA-1 compression
        key = QCryptographicHash::hash(key, QCryptographicHash::Sha1);
    }
    QByteArray innerPadding(blockSize, char(0x36)); // initialize inner padding with char "6"
    QByteArray outerPadding(blockSize, char(0x5c)); // initialize outer padding with char "/"
    // ascii characters 0x36 ("6") and 0x5c ("/") are selected because they have large
    // Hamming distance (http://en.wikipedia.org/wiki/Hamming_distance)
    for (int i = 0; i < key.length(); i++) {
        innerPadding[i] = innerPadding[i] ^ key.at(i); // XOR operation between every byte in key and innerpadding, of key length
        outerPadding[i] = outerPadding[i] ^ key.at(i); // XOR operation between every byte in key and outerpadding, of key length
    }
    // result = hash ( outerPadding CONCAT hash ( innerPadding CONCAT baseString ) ).toBase64
    QByteArray total = outerPadding;
    QByteArray part = innerPadding;
    part.append(baseString);
    total.append(QCryptographicHash::hash(part, QCryptographicHash::Sha1));
    QByteArray hashed = QCryptographicHash::hash(total, QCryptographicHash::Sha1);
    return hashed.toBase64();
}
